let hamburger = document.getElementById("hamburger");
let links = document.getElementById("links");
let main = document.querySelector("main");

hamburger.addEventListener("click", hideLinks);
hamburger.addEventListener("click", changeMenuState);

let link = document.querySelectorAll("ul#links > *");
for (let i = 0; i < link.length; i++) {
	let allLinks = link[i];
	allLinks.addEventListener("click", hideLinks);
	allLinks.addEventListener("click", removeActive);
}

main.addEventListener("click", hideLinksOnly);
main.addEventListener("click", removeActive);

function hideLinks() {
	if (links.classList.contains("hidden")) {
		links.classList.remove("hidden");
	} else {
		links.classList.add("hidden");
	}
}
function hideLinksOnly() {
	if (links.classList.contains("hidden") === false) {
		links.classList.add("hidden");
	}
}
function changeMenuState() {
	hamburger.classList.toggle("active");
}
function removeActive() {
	hamburger.classList.remove("active");
}

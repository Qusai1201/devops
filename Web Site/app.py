from flask import Flask, render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re
import os
import base64
import uuid

from tensorflow import keras
import tensorflow as tf
from PIL import Image
import cv2
import numpy as np
####



img_height,img_width = 224,224 

resnet50_model = keras.models.load_model(r'models/mushrooms.h5')
class_names = ['Agaricus', 'Amanita', 'Boletus', 'Entoloma', 'Exidia', 'Hygrocybe', 'Inocybe', 'Lactarius', 'Russula']
# #####


app = Flask(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = True

app.secret_key = 'secret_key'
app.config['MYSQL_HOST'] = '172.17.0.2'
app.config['MYSQL_USER'] = 'qusai'  
app.config['MYSQL_PASSWORD'] = 'qusai'
app.config['MYSQL_DB'] = 'mushroom'

mysql = MySQL(app)


def predict(image):
	image_resized= cv2.resize(image, (img_height,img_width))
	image=np.expand_dims(image_resized,axis=0)
	predRESNET=resnet50_model.predict(image)
	item = class_names[np.argmax(predRESNET)] 
	print(item)
	prop = {
		"Agaricus" :" it's edible.",
		"Amanita" :" Amanita phalloides, also known as 'death cap', is one of the most poisonous mushrooms",
		"Boletus" :" The majority of boletes are edible",
		"Entoloma" :" is a large and very poisonous species",
		"Exidia" :" it's edible.",
		"Hygrocybe" :" Many of these species are not necessarily poisonous",
		"Inocybe" :" Inocybe mushrooms contain toxic substances, which can cause poisoning at low dose.",
		"Lactarius" :" Some Lactarius are considered toxic",
		"Russula" :" is mostly free of deadly poisonous species"
	}
	return item,prop[item]

######

@app.route('/')
@app.route('/index')
def index():
	if session and session['id'] :
		return render_template('index.html')
	else:
		msg = 'Please Login First !'
		return render_template('login.html', msg = msg)

@app.route('/login', methods =['GET', 'POST'])
def login():
	if session and session['id'] :
		return redirect(url_for('index'))
	msg = ''
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
		username = request.form['username']
		password = request.form['password']
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM users WHERE username = % s AND password = % s', (username, password, ))
		account = cursor.fetchone()
		if account:
			session['loggedin'] = True
			session['id'] = account['id']
			session['username'] = account['username']
			return redirect(url_for('index'))
		else:
			msg = 'Incorrect username / password !'
	return render_template('login.html', msg = msg)

@app.route('/logout')
def logout():
	session.pop('loggedin', None)
	session.pop('id', None)
	session.pop('username', None)
	return redirect(url_for('login'))

@app.route('/register', methods =['GET', 'POST'])
def register():
	if session and session['id'] :
		return redirect(url_for('index'))
	msg = ''
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form :
		username = request.form['username']
		password = request.form['password']
		email = request.form['email']
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM users WHERE username = % s', (username, ))
		account = cursor.fetchone()
		if account:
			msg = 'Account already exists !'
		elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
			msg = 'Invalid email address !'
		elif not re.match(r'[A-Za-z0-9]+', username):
			msg = 'Username must contain only characters and numbers !'
		elif not username or not password or not email:
			msg = 'Please fill out the form !'
		else:
			cursor.execute('INSERT INTO users VALUES (NULL, % s, % s, % s)', (username, password, email, ))
			mysql.connection.commit()
			msg = 'You have successfully registered !'
	elif request.method == 'POST':
		msg = 'Please fill out the form !'
	return render_template('register.html', msg = msg)

@app.route('/upload', methods =['POST'])
def upload():
	if request.files and request.files['image']:
		name = uuid.uuid4().hex
		request.files["image"].save('./images/'+name)
		image = cv2.imread( './images/'+name , cv2.IMREAD_UNCHANGED)
		msg = {'mushroom':predict(image=image)}
		jpg_img = cv2.imencode('.jpg', image)
		b64_string = base64.b64encode(jpg_img[1]).decode('utf-8')
		return render_template('index.html', msg = msg,img = "data:image/jpg;base64,"+b64_string)
	else:
		msg = {'error':'Please select a image file to predict'}
		return render_template('index.html', msg = msg)

os.system("powershell Remove-Item ./images/* -Recurse -Force")
